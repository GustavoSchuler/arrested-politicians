<?php

	function createConnection($db) {

		$_SERVER = "localhost";
		$_USERNAME = "root";
		$_PASSWORD = "";
		$_DATABASE = $db;

		// Create connection
		if ($db == null) {
			$conn = new mysqli($_SERVER, $_USERNAME, $_PASSWORD);
		} else {
			$conn = new mysqli($_SERVER, $_USERNAME, $_PASSWORD, $_DATABASE);
		}
		// Check connection
		if ($conn->connect_error) {
		    die("Connection failed: " . $conn->connect_error);
		}

		return $conn;

	}

?>
<?php

	session_start();
  	$_SESSION['SID'] = session_id();
	$user = $_SESSION['user'];

	if (isset($_SESSION["logged"])){
		if ($_SESSION["logged"] == 0){
		  header("Location:/login.php");
		}
	}else{
		header("Location:/login.php");
	}

	require "controllers/politician.php";

?>

<!DOCTYPE html>

<html>

<head>

  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

  <title>Arrested Politicians</title>
  <link rel=icon href=assets/images/logo.png sizes="16x16" type="image/png">

  <link rel="stylesheet" type="text/css" href="assets/dist/components/reset.css">
  <link rel="stylesheet" type="text/css" href="assets/dist/components/site.css">

  <link rel="stylesheet" type="text/css" href="assets/dist/components/container.css">
  <link rel="stylesheet" type="text/css" href="assets/dist/components/grid.css">
  <link rel="stylesheet" type="text/css" href="assets/dist/components/header.css">
  <link rel="stylesheet" type="text/css" href="assets/dist/components/image.css">
  <link rel="stylesheet" type="text/css" href="assets/dist/components/menu.css">

  <link rel="stylesheet" type="text/css" href="assets/dist/components/divider.css">
  <link rel="stylesheet" type="text/css" href="assets/dist/components/dropdown.css">
  <link rel="stylesheet" type="text/css" href="assets/dist/components/segment.css">
  <link rel="stylesheet" type="text/css" href="assets/dist/components/button.css">
  <link rel="stylesheet" type="text/css" href="assets/dist/components/list.css">
  <link rel="stylesheet" type="text/css" href="assets/dist/components/icon.css">
  <link rel="stylesheet" type="text/css" href="assets/dist/components/sidebar.css">
  <link rel="stylesheet" type="text/css" href="assets/dist/components/transition.css">
  <link rel="stylesheet" type="text/css" href="assets/dist/components/table.css">
  <link rel="stylesheet" type="text/css" href="assets/library/sweetalert/sweetalert2.min.css">

  <link rel="stylesheet" type="text/css" href="assets/css/custom.css">  

</head>
<body class="pushable">

<!-- Following Menu -->
<div class="ui large top fixed menu transition hidden">
  <div class="ui container">
    <a class="item" href="/">Home</a>
    <a class="item" href="/list.php">Arrested Politicians</a>
    <a class="item">About Us</a>
    <a class="item">Contact</a>
    <div class="right menu">
      <?php
        if (isset($_SESSION['logged'])){
          if ($_SESSION['logged'] == 0) {
            echo '<div class="item"><a class="ui primary button" href="login.php">Log in</a></div>';
          } else {
            echo '<a class="item">' . $_SESSION['user'] . '</a><div class="item"><a class="ui primary button" href="admin.php">Admin</a></div><div class="item"><a class="ui primary button" href="controllers/logout.php">Logout</a></div>';
          }
        } else {
          echo '<div class="item"><a class="ui primary button" href="login.php">Log in</a></div>';
        }
      ?>
    </div>
  </div>
</div>

<!-- Sidebar Menu -->
<div class="ui vertical inverted sidebar menu left">
  <a class="item" href="/">Home</a>
  <a class="item" href="/list.php">Arrested Politicians</a>
  <a class="item">About Us</a>
  <a class="item">Contact</a>
  <?php
    if (isset($_SESSION['logged'])){
      if ($_SESSION['logged'] == 0) {
        echo '<a class="item" href="login.php">Login</a>';
      } else {
        echo '<a class="item" href="admin.php">Admin</a><a class="item" href="controllers/logout.php">Logout</a>';
      }
    } else {
      echo '<a class="item" href="login.php">Login</a>';
    }
  ?>
</div>


<!-- Page Contents -->
<div class="pusher">
  <div class="ui inverted vertical masthead center aligned segment">

    <div class="ui container">
      <div class="ui large secondary inverted pointing menu">
        <a class="toc item">
          <i class="sidebar icon"></i>
        </a>
        <a class="item" href="/">Home</a>
        <a class="item" href="/list.php">Arrested Politicians</a>
        <a class="item">About Us</a>
        <a class="item">Contact</a>
        <div class="right item">
          <?php
            if (isset($_SESSION['logged'])){
              if ($_SESSION['logged'] == 0) {
                echo '<a class="ui inverted button" href="login.php">Login</a>';
              } else {
                echo '<a class="ui inverted"><span>' . $_SESSION['user'] . '</span></a><a class="ui inverted button" href="admin.php">Admin</a><a class="ui inverted button" href="controllers/logout.php">Logout</a>';
              }
            } else {
              echo '<a class="ui inverted button" href="login.php">Login</a>';
            }
          ?>
        </div>
      </div>
    </div>

    <div class="ui text container">
      <h1 class="ui inverted header">
        Admin area
      </h1>
      <h2>Here the things get done.</h2>
    </div>

  </div>

  <div class="ui vertical stripe segment">
  	<div class="ui middle aligned stackable grid container">

    <form id="frm1" action="" method="post">
      <input type="button" class="ui primary small button" data-id="" name="action" value="Add" />
    </form>

  		<?php

        echo '<table class="ui celled table">
                <thead>
                  <tr>
                    <th style="text-align:center">Picture</th>
                    <th style="text-align:center">Name</th>
                    <th style="text-align:center">Description</th>
                    <th style="text-align:center">Date Arrested</th>
                    <th style="text-align:center"></th>
                    <th style="text-align:center"></th>
                  </tr>
                </thead>
                <tbody>';

        $list = getPoliticiansList();

        if ($list != false){

            for ($i=0; $i<count($list); $i++){

                $date = $list[$i]->getArrestDate();
                $date = date_create($date);
                $date = date_format($date,"d/m/Y");

                echo '<form id="frm" action="" method="post">
                        <tr>
                          <td align="center"><img class="ui tiny image" src="assets/images/' . $list[$i]->getPicture() . '" border="0" width="80px" height="60px"></td>
                          <td align="center">' . $list[$i]->getName() . '</td>
                          <td align="center">' . $list[$i]->getDescription() . '</td>
                          <td align="center">' . $date . '</td>
                          <td align="center">
                              <input type="button" class="ui primary small button" name="action" data-id="' . $list[$i]->getId() . '" value="Edit" />
                          </td>
                          <td align="center">
                              <input type="button" class="ui negative small button" name="action" data-id="' . $list[$i]->getId() . '" value="Remove" />
                          </td>
                        </tr>
                      </form>';
            }
        } else {
          echo '<tr><td><p>Nenhum registro encontrado.</p></td></tr>';
        }

      ?>

      </tbody></table> 

  	</div>
  </div>


  <div class="ui inverted vertical footer segment">
    <div class="ui container">
      <div class="ui stackable inverted divided equal height stackable grid">
        <div class="three wide column">
          <h4 class="ui inverted header">About</h4>
          <div class="ui inverted link list">
            <a href="#" class="item">Sitemap</a>
            <a href="#" class="item">Contact Us</a>
          </div>
        </div>
        <div class="three wide column">
          <h4 class="ui inverted header">BlaBlaBla</h4>
          <div class="ui inverted link list">
            <a href="#" class="item">Privacy Policy</a>
            <a href="#" class="item">FAQ</a>
          </div>
        </div>
        <div class="seven wide column">
          <h4 class="ui inverted header">Arrested Politicians</h4>
          <p>Oh, my! They're arrested!<br/>Look at the pretty face of the cause of your problems.</p>
        </div>
      </div>
    </div>
  </div>
</div>


<script src="assets/library/jquery.min.js"></script>
<script src="assets/dist/components/visibility.js"></script>
<script src="assets/dist/components/sidebar.js"></script>
<script src="assets/dist/components/transition.js"></script>
<script src="assets/library/sweetalert/sweetalert2.min.js"></script>
<script>

  $(document)
    .ready(function() {

      // fix menu when passed
      $('.masthead')
        .visibility({
          once: false,
          onBottomPassed: function() {
            $('.fixed.menu').transition('fade in');
          },
          onBottomPassedReverse: function() {
            $('.fixed.menu').transition('fade out');
          }
        })
      ;

      // create sidebar and attach to menu open
      $('.ui.sidebar').sidebar('attach events', '.toc.item');

      $('[name="action"]').click(function(){
          
          var action = $(this).val();
          var id = $(this).attr('data-id');
          var ajaxurl = 'controllers/politician.php',

          data =  {'action': action, 'id': id};
          
          $.post(ajaxurl, data, function (response) {

              var res = jQuery.parseJSON(response);

              if (res.message) {

                swal({   
                      title: "Arrested Politicians",   
                      text: res.message,   
                      type: res.type,   
                      showCancelButton: false,     
                      confirmButtonText: "Ok"
                    }).then(function() {
                      location.reload();
                    });

              } else {

                if(!res.redirect) {

                  setTimeout(function() {
                     location.reload();
                  }, 0001);

                } else {

                  window.location.assign(res.location);

                }

              }
              
          });

      });

    });

</script>

</body>
</html>
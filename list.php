<?php

  session_start();
  $_SESSION['SID'] = session_id();

  require "controllers/politician.php";

?>

<!DOCTYPE html>
<html lang="pt-BR" class="no-js">
<head>
  <!-- Standard Meta -->
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1; charset=UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

  <!-- Site Properties -->
  <title>Arrested Politicians</title>
  <link rel=icon href=assets/images/logo.png sizes="16x16" type="image/png">

  <link rel="stylesheet" type="text/css" href="assets/dist/components/reset.css">
  <link rel="stylesheet" type="text/css" href="assets/dist/components/site.css">

  <link rel="stylesheet" type="text/css" href="assets/dist/components/container.css">
  <link rel="stylesheet" type="text/css" href="assets/dist/components/grid.css">
  <link rel="stylesheet" type="text/css" href="assets/dist/components/header.css">
  <link rel="stylesheet" type="text/css" href="assets/dist/components/image.css">
  <link rel="stylesheet" type="text/css" href="assets/dist/components/menu.css">

  <link rel="stylesheet" type="text/css" href="assets/dist/components/divider.css">
  <link rel="stylesheet" type="text/css" href="assets/dist/components/list.css">
  <link rel="stylesheet" type="text/css" href="assets/dist/components/segment.css">
  <link rel="stylesheet" type="text/css" href="assets/dist/components/button.css">
  <link rel="stylesheet" type="text/css" href="assets/dist/components/dropdown.css">
  <link rel="stylesheet" type="text/css" href="assets/dist/components/sidebar.css">
  <link rel="stylesheet" type="text/css" href="assets/dist/components/icon.css">
  <link rel="stylesheet" type="text/css" href="assets/dist/components/transition.css">

  <link rel="stylesheet" type="text/css" href="assets/css/reset.css"> 
  <link rel="stylesheet" type="text/css" href="assets/css/custom.css"> 

</head>
<body class="pushable">

  <!-- Following Menu -->
  <div class="ui large top fixed menu transition hidden">
    <div class="ui container">
      <a class="item" href="../">Home</a>
      <a class="active item">Arrested Politicians</a>
      <a class="item">About Us</a>
      <a class="item">Contact</a>
      <div class="right menu">
        <?php
          if (isset($_SESSION['logged'])){
            if ($_SESSION['logged'] == 0) {
              echo '<div class="item"><a class="ui primary button" href="login.php">Log in</a></div>';
            } else {
              echo '<a class="item">' . $_SESSION['user'] . '</a><div class="item"><a class="ui primary button" href="admin.php">Admin</a></div><div class="item"><a class="ui primary button" href="controllers/logout.php">Logout</a></div>';
            }
          } else {
            echo '<div class="item"><a class="ui primary button" href="login.php">Log in</a></div>';
          }
        ?>
      </div>
    </div>
  </div>

  <!-- Sidebar Menu -->
  <div class="ui vertical inverted sidebar menu left">
    <a class="item" href="../">Home</a>
    <a class="active item">Arrested Politicians</a>
    <a class="item">About Us</a>
    <a class="item">Contact</a>
    <?php
      if (isset($_SESSION['logged'])){
        if ($_SESSION['logged'] == 0) {
          echo '<a class="item" href="login.php">Login</a>';
        } else {
          echo '<a class="item" href="admin.php">Admin</a><a class="item" href="controllers/logout.php">Logout</a>';
        }
      } else {
        echo '<a class="item" href="login.php">Login</a>';
      }
    ?>
  </div>

  <!-- Page Contents -->
  <div class="pusher" style="background-color: #f2f2f2">
    <div class="ui inverted vertical masthead center aligned segment">

      <div class="ui container">
        <div class="ui large secondary inverted pointing menu">
          <a class="toc item">
            <i class="sidebar icon"></i>
          </a>
          <a class="item" href="../">Home</a>
          <a class="active item">Arrested Politicians</a>
          <a class="item">About Us</a>
          <a class="item">Contact</a>
          <div class="right item">
            <?php
              if (isset($_SESSION['logged'])){
                if ($_SESSION['logged'] == 0) {
                  echo '<a class="ui inverted button" href="login.php">Login</a>';
                } else {
                  echo '<a class="ui inverted"><span>' . $_SESSION['user'] . '</span></a><a class="ui inverted button" href="admin.php">Admin</a><a class="ui inverted button" href="controllers/logout.php">Logout</a>';
                }
              } else {
                echo '<a class="ui inverted button" href="login.php">Login</a>';
              }
            ?>
          </div>
        </div>
      </div>

      <div class="ui text container">
        <h1 class="ui inverted header">
          There is.
        </h1>
      </div>

    </div>

    <div class="ui vertical stripe segment">
      <div class="ui middle aligned stackable grid container">
        <section id="cd-timeline" class="cd-container">

          <?php 

            $list = getPoliticiansList();

            for ($i=0; $i<count($list); $i++) {

                $politician = $list[$i];

                $date = $politician->getArrestDate();
                $date = date_create($date);
                $date = date_format($date,"d/m/Y");

                echo '<div class="cd-timeline-block">
                        <div class="cd-timeline-img cd-picture">
                          <img src="assets/images/handcuffs.svg" alt="Picture">
                        </div>

                        <div class="cd-timeline-content">
                          <h2>' . $politician->getName() . '</h2>
                          <p>' . $politician->getDescription() . '</p>
                          <span class="cd-date">' . $date . '</span>
                          <img src="assets/images/' . $politician->getPicture() . '">
                        </div>
                      </div>';
            }

          ?>

        </section>
      </div>
    </div>

    <div class="ui inverted vertical footer segment">
      <div class="ui container">
        <div class="ui stackable inverted divided equal height stackable grid">
          <div class="three wide column">
            <h4 class="ui inverted header">About</h4>
            <div class="ui inverted link list">
              <a href="#" class="item">Sitemap</a>
              <a href="#" class="item">Contact Us</a>
            </div>
          </div>
          <div class="three wide column">
            <h4 class="ui inverted header">BlaBlaBla</h4>
            <div class="ui inverted link list">
              <a href="#" class="item">Privacy Policy</a>
              <a href="#" class="item">FAQ</a>
            </div>
          </div>
          <div class="seven wide column">
            <h4 class="ui inverted header">Arrested Politicians</h4>
            <p>Oh, my! They're arrested!<br/>Look at the pretty face of the cause of your problems.</p>
          </div>
        </div>
      </div>
    </div>
  </div>

<script src="assets/library/jquery.min.js"></script>

<script src="assets/library/modernizr.js"></script>
<script src="assets/library/main.js"></script>

<script src="assets/dist/components/visibility.js"></script>
<script src="assets/dist/components/sidebar.js"></script>
<script src="assets/dist/components/transition.js"></script>

<script>

  $(document)
    .ready(function() {

      // fix menu when passed
      $('.masthead')
        .visibility({
          once: false,
          onBottomPassed: function() {
            $('.fixed.menu').transition('fade in');
          },
          onBottomPassedReverse: function() {
            $('.fixed.menu').transition('fade out');
          }
        })
      ;

      // create sidebar and attach to menu open
      $('.ui.sidebar').sidebar('attach events', '.toc.item');

    });

</script>

</body>

</html>
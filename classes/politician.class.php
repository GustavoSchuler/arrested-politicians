<?php

class Politician {

  private $id;
  private $name;
  private $description;
  private $picture;
  private $arrest_date;
  
  function __construct ($_id, $_name, $_description, $_picture, $_arrest_date) {
  	$this->id = $_id;
    $this->name = $_name;
  	$this->description = $_description;
    $this->picture = $_picture;
    $this->arrest_date = $_arrest_date;
  }

  function setId($_id) {
    $this->id = $_id;
  }

  function getId() {
    return $this->id;
  }

  function setName($_name) {
	  $this->name = $_name;
  }

  function getName() {
	  return $this->name;
  }
 
  function setDescription($_description) {
    $this->description = $_description;
  }

  function getDescription() {
    return $this->description;
  }

  function setPicture($_picture) {
    $this->picture = $_picture;
  }

  function getPicture() {
    return $this->picture;
  }

  function setArrestDate($_arrest_date) {
    $this->arrest_date = $_arrest_date;
  }

  function getArrestDate() {
    return $this->arrest_date;
  }

}
<?php

class User {

  private $email;
  private $password;
  
  function __construct ($_email, $_password) {
  	$this->email = $_email;
  	$this->password = $_password;
  }

  function setEmail($_email) {
	  $this->email = $_email;
  }
  function getEmail() {
	  return $this->email;
  }
 
  function setPassword($_password) {
    $this->password = $_password;
  }

  function getPassword() {
    return $this->password;
  }

}
<!DOCTYPE html>
<html>
<head>
  <!-- Standard Meta -->
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

  <!-- Site Properties -->
  <title>Login - Arrested Politicians</title>
  <link rel=icon href=assets/images/logo.png sizes="16x16" type="image/png">
  
  <link rel="stylesheet" type="text/css" href="assets/dist/components/reset.css">
  <link rel="stylesheet" type="text/css" href="assets/dist/components/site.css">

  <link rel="stylesheet" type="text/css" href="assets/dist/components/container.css">
  <link rel="stylesheet" type="text/css" href="assets/dist/components/grid.css">
  <link rel="stylesheet" type="text/css" href="assets/dist/components/header.css">
  <link rel="stylesheet" type="text/css" href="assets/dist/components/image.css">
  <link rel="stylesheet" type="text/css" href="assets/dist/components/menu.css">

  <link rel="stylesheet" type="text/css" href="assets/dist/components/divider.css">
  <link rel="stylesheet" type="text/css" href="assets/dist/components/segment.css">
  <link rel="stylesheet" type="text/css" href="assets/dist/components/form.css">
  <link rel="stylesheet" type="text/css" href="assets/dist/components/input.css">
  <link rel="stylesheet" type="text/css" href="assets/dist/components/button.css">
  <link rel="stylesheet" type="text/css" href="assets/dist/components/list.css">
  <link rel="stylesheet" type="text/css" href="assets/dist/components/message.css">
  <link rel="stylesheet" type="text/css" href="assets/dist/components/icon.css">

  <script src="assets/library/jquery.min.js"></script>
  <script src="assets/dist/components/form.js"></script>
  <script src="assets/dist/components/transition.js"></script>

  <style type="text/css">
    body {
      background-color: #DADADA;
    }
    body > .grid {
      height: 100%;
    }
    .image {
      margin-top: -100px;
    }
    .column {
      max-width: 450px;
    }
  </style>
  <script>
  $(document)
    .ready(function() {
      $('.ui.form')
        .form({
          fields: {
            email: {
              identifier  : 'email',
              rules: [
                {
                  type   : 'empty',
                  prompt : 'Please enter your e-mail'
                },
                {
                  type   : 'email',
                  prompt : 'Please enter a valid e-mail'
                }
              ]
            },
            password: {
              identifier  : 'password',
              rules: [
                {
                  type   : 'empty',
                  prompt : 'Please enter your password'
                },
                {
                  type   : 'length[4]',
                  prompt : 'Your password must be at least 4 characters'
                }
              ]
            }
          }
        })
      ;
    })
  ;
  </script>
</head>
<body>

<div class="ui middle aligned center aligned grid">
  <div class="column">
    <h2 class="ui blue image header">
      <img src="assets/images/logo.png" class="image">
      <div class="content">
        Log-in for site administrators
      </div>
    </h2>
    <form class="ui large form" action="/controllers/auth.php" method="post">
      <div class="ui stacked segment">
        <div class="field">
          <div class="ui left icon input">
            <i class="user icon"></i>
            <input type="text" name="email" placeholder="E-mail address">
          </div>
        </div>
        <div class="field">
          <div class="ui left icon input">
            <i class="lock icon"></i>
            <input type="password" name="password" placeholder="Password">
          </div>
        </div>
        <div class="ui fluid large blue submit button">Login</div>
      </div>

      <div class="ui error message"></div>

      <?php
        session_start();
        if (isset($_SESSION["logged"])){
          if ($_SESSION["logged"] == 0){
            echo '<div class="alert alert-danger alert-dismissible" role="alert"> 
                    <button type="button" class="close" data-dismiss="alert">
                      <span aria-hidden="true">&times;</span>
                      <span class="sr-only">Close</span>
                    </button>
                    Invalid User or Password!
                  </div>';
            session_destroy();
          }
        }
      ?>

    </form>

  </div>
</div>

</body>

</html>
